import { IsNotEmpty, IsDateString, IsOptional } from "class-validator"

export class UpdateTaskDto {
  @IsOptional()
  @IsNotEmpty()
  description?: string

  @IsOptional()
  @IsDateString()
  deadline?: Date

  @IsOptional()
  status?: string
}
