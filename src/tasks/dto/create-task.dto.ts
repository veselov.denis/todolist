import { IsNotEmpty, IsDateString } from "class-validator"

export class CreateTaskDto {
  @IsNotEmpty()
  description: string

  @IsDateString()
  deadline: Date
}
