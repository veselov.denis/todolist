import { Prop, Schema, SchemaFactory } from "@nestjs/mongoose"
import { Document } from "mongoose"

@Schema()
export class Task extends Document {
  @Prop({ required: true })
  description: string

  @Prop({ required: true })
  deadline: Date

  @Prop({ required: true })
  status: string
}

export const TaskSchema = SchemaFactory.createForClass(Task)
