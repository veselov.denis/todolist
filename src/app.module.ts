import { Module } from "@nestjs/common"
import { MongooseModule } from "@nestjs/mongoose"
import * as process from "process"
import { TasksController } from "./tasks/tasks.controller"
import { TasksService } from "./tasks/tasks.service"
import { Task, TaskSchema } from "./tasks/schemas/task.schema"

@Module({
  imports: [
    MongooseModule.forRoot(
      process.env.MONGODB_URI || "mongodb://localhost:27017/todolistdbname"
    ),
    MongooseModule.forFeature([{ name: Task.name, schema: TaskSchema }]),
  ],
  controllers: [TasksController],
  providers: [TasksService],
})
export class AppModule {}
